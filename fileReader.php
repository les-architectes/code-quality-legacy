<?php
function headersExist($key, $header, $text){
    if (array_key_exists($key, $header)){
        if (is_array($header[$key])){
            $array_header = $header[$key];
            $array_length = count($array_header);
            for ($i = 0; $i <= $array_length - 1 ; $i++) {
                    print_r($array_header[$i] . ', ');
            }
        }
        else{
            print_r($header[$key]);
        }
    }
    else {
        print_r($text);
    }
    echo "\n";
}

function urlHeaders($url)
{
    $headers = get_headers($url, 1);
    headersExist('Content-Type', $headers, 'NO CONTENT-TYPE');
    headersExist('Expires', $headers, 'NO EXPIRES');
    headersExist('Cache-Control', $headers, 'NO CACHE-CONTROL');
}

function read($csv){

    $file = fopen($csv, 'r');
    while (!feof($file) ) {
        $line[] = fgetcsv($file, 1024);
    }
    fclose($file);
    return $line;
}

$csv = 'url.csv';
$csv = read($csv);
foreach ($csv as $line) {
    echo "\n";
    print_r($line[1]);
    echo "\n";
    print_r($line[0]);
    echo "\n";
    urlHeaders($line[0]);
    echo "\n";
}

