# Code Quality

- Parcourir un fichier CSV qui contient des urls.
- Pour chaque URL, récupérer les entetes de site associé.
- Afficher les en-têtes 'cache_control', 'content_type' et 'expires' si elles existent.
